<div class="noti-height">
	<div class="container">
		<p class="tieude-tkb full-width">THÔNG BÁO CHUNG</p>
		<div class="slideup full-width">
		<?php if($items){ ?>
			<?php foreach ($items as $k => $v): ?>
				<div class="div-jquery" id="notifi-custom">
				    <h4 class="slide-notificate">
				        <span><?= $v->title; ?> (<?= $v->created; ?>)</span>
				        <i class="arrow"></i>
				    </h4>
				    <article>
				        <div class="text-description">
				        	<?= $v->content; ?>
				        </div>
				    </article>
				</div>
			<?php endforeach; ?>
		<?php } ?>
		</div>
	</div>
</div>

