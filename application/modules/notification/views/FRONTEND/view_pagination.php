<?php if($items){ ?>
<?php foreach ($items as $k => $v): ?>
	<div class="div-student full-width">
	    <h4 class="quote-icon full-width">
	        <p class="student-name"><?= $v->title; ?></p>
	        <i class="arrow"></i>
	    </h4><!-- quote-icon -->
	    <article class="full-width">
	    	<img class="img-responsive students-thumb" src="<?=resizeImage(PATH_URL.DIR_UPLOAD_NOTIFICATION.$v->image,110, 110)?>">
	        <div class="cmt-text"><i class="cmt-top"></i><?= $v->content; ?><i class="cmt-bottom"></i></div>
	    </article>
	</div><!-- div-student -->
<?php endforeach; ?>
<?php } else { ?>
    <?php echo('データがありません。'); ?>
<?php } ?>

<div class="pagination-custom full-width">
	<ul class="pagination">
	  	<?=$this->adminpagination->create_links();?>
	</ul>
</div>