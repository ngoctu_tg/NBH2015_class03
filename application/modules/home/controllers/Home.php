<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {

	function __construct(){
		parent::__construct();
	}
	
	/*------------------------------------ FRONTEND ------------------------------------*/
	public function index(){
		$this->load->model('News/News_model');
		// get banners
		$data['news_highlight'] = $this->News_model->getDataHighlight(4);
		$data['home_page'] = true;
		$this->template->write('title','Căn Hộ Đầu tiên');
		$this->template->write('meta_keywords','Căn Hộ Đầu tiên');
		$this->template->write('meta_description','Căn Hộ Đầu tiên');
		$this->template->write('meta_url', PATH_URL);
		$this->template->write_view('content','index', $data);
		$this->template->render();
	}

	public function contact(){
		// get banners
		$this->template->write('title','NhatBanHocN3');
		$this->template->write('meta_keywords','NhatBanHocN3');
		$this->template->write('meta_description','NhatBanHocN3');
		$this->template->write('meta_url', PATH_URL);
		$this->template->write_view('content','contact');
		$this->template->render();
	}

	public function send_email(){
		
	}

	public function loadThoiKhoaBieu() {
		$this->load->model('static_pages/static_pages_model');
		$data["thoikhoabieu"] = $this->static_pages_model->getData("thoikhoabieu");
		$this->template->write_view('content','templateThoiKhoaBieu',$data);
		$this->template->render();
	}

	// public function loadThoiKhoaBieu(){
	// 	$this->load->model('static_pages/static_pages_model');
	// 	$data["content"] = $this->static_pages_model->getData("thoikhhoabieu");
	// 	$this->load->view("templateThoiKhoaBieu", $data);
	// }

	public function loadDanhSachSV() {
		$this->load->model('static_pages/static_pages_model');
		$data["danhsachsv"] = $this->static_pages_model->getData("danh-sach-sinh-vien");
		$this->template->write_view('content','templateDanhSachSV',$data);
		$this->template->render();
	}

	public function loadBieuMau() {
		$this->load->model('static_pages/static_pages_model');
		$data["thoikhoabieu"] = $this->static_pages_model->getData("bieu-mau");
		$this->template->write_view('content','templateBieuMau',$data);
		$this->template->render();
	}

	public function loadLienHe() {
		$this->load->model('static_pages/static_pages_model');
		$data["thoikhoabieu"] = $this->static_pages_model->getData("lien-he");
		$this->template->write_view('content','templateLienHe',$data);
		$this->template->render();
	}


	/*------------------------------------ End FRONTEND --------------------------------*/
}