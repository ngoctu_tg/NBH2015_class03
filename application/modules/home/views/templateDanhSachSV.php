<div class="container">
	<!--<?php 
		echo parserEditorHTML($danhsachsv[0]->content);
	?>-->
	<p class="tieude-tkb full-width">DANH SÁCH LỚP N3 (KHÓA 2015)</p>
	<p class="full-width" style="margin-bottom: 20px;text-align: center;padding: 0px 70px; font-size: 20px;">Danh sách chính thức của lớp căn cứ vào danh sách đóng tiền tạm ứng và làm hồ sơ nhập học. 
	Mọi người vui lòng kiểm tra kỹ thông tin. Nếu có gì nhầm lẫn xin vui lòng phản hồi càng sớm càng tốt.</p>
	<div class="list-dssv full-width">
		<!-- <ul class="header-label">
			<li class="stt">STT</li>
			<li class="name">HỌ TÊN</li>
			<li class="gender">PHÁI</li>
			<li class="birthday">NGÀY SINH</li>
			<li class="note">MSSV</li>
		</ul> -->
		<div id="content-1" class="content full-width">
			<table class="table-dssv full-width">
				<tr>
					<th class="col1">STT</th>
					<th class="col2">HỌ TÊN</th>
					<th class="col3">PHÁI</th>
					<th class="col4">NGÀY SINH</th>
					<th class="col5">MSSV</th>
				</tr>
				<tr>
					<td class="col1">1</td>
					<td class="col2">Huỳnh Thị Ngọc Ánh</td>
					<td class="col3">Nữ</td>
					<td class="col4">10/30/1991</td>
					<td class="col5">1566190005</td>
				</tr>
				
				<tr>
					<td class="col1">2</td>
					<td class="col2">Nguyễn Hùng Duy</td>
					<td class="col3">Nam</td>
					<td class="col4">7/27/1988</td>
					<td class="col5">1566190019</td>
				</tr>
				<tr>
					<td class="col1">3</td>
					<td class="col2">Lê Thùy Giang</td>
					<td class="col3">Nữ</td>
					<td class="col4">6/26/1990</td>
					<td class="col5">1566190021</td>
				</tr>
				<tr>
					<td class="col1">4</td>
					<td class="col2">Phạm Ngọc Hà</td>
					<td class="col3">Nam</td>
					<td class="col4">10/16/1988</td>
					<td class="col5">1566190023</td>
				</tr>
				<tr>
					<td class="col1">5</td>
					<td class="col2">Nguyễn Thúy Hằng</td>
					<td class="col3">Nữ</td>
					<td class="col4">7/7/1990</td>
					<td class="col5">1566190028</td>
				</tr>
				<tr>
					<td class="col1">6</td>
					<td class="col2">Trịnh Thị Mỹ Hiệp</td>
					<td class="col3">Nữ</td>
					<td class="col4">9/22/1978</td>
					<td class="col5">1566190034</td>
				</tr>
				<tr>
					<td class="col1">7</td>
					<td class="col2">Trần Ngọc Tuyết Khanh</td>
					<td class="col3">Nữ</td>
					<td class="col4">7/30/1979</td>
					<td class="col5">1566190040</td>
				</tr>
				<tr>
					<td class="col1">8</td>
					<td class="col2">Nguyễn Hải Linh</td>
					<td class="col3">Nam</td>
					<td class="col4">11/3/1982</td>
					<td class="col5">1566190047</td>
				</tr>
				<tr>
					<td class="col1">9</td>
					<td class="col2">Trần Thị Xuân Mai</td>
					<td class="col3">Nữ</td>
					<td class="col4">1/30/1987</td>
					<td class="col5">1566190055</td>
				</tr>
				<tr>
					<td class="col1">10</td>
					<td class="col2">Trần Kim Ngọc</td>
					<td class="col3">Nữ</td>
					<td class="col4">11/19/1985</td>
					<td class="col5">1566190066</td>
				</tr>
				<tr>
					<td class="col1">11</td>
					<td class="col2">Nguyễn Ngọc Yến Nhi</td>
					<td class="col3">Nữ</td>
					<td class="col4">12/5/1993</td>
					<td class="col5">1566190070</td>
				</tr>
				<tr>
					<td class="col1">12</td>
					<td class="col2">Phạm Thị Phép</td>
					<td class="col3">Nữ</td>
					<td class="col4">11/27/1991</td>
					<td class="col5">1566190075</td>
				</tr>
				<tr>
					<td class="col1">13</td>
					<td class="col2">Nguyễn Đức Quang</td>
					<td class="col3">Nam</td>
					<td class="col4">9/28/1970</td>
					<td class="col5">1566190079</td>
				</tr>
				<tr>
					<td class="col1">14</td>
					<td class="col2">Nguyễn Thị Cẩm Quyên</td>
					<td class="col3">Nữ</td>
					<td class="col4">12/12/1992</td>
					<td class="col5">1566190082</td>
				</tr>
				<tr>
					<td class="col1">15</td>
					<td class="col2">Dương Thị Lê Quyên</td>
					<td class="col3">Nữ</td>
					<td class="col4">10/14/1989</td>
					<td class="col5">1566190081</td>
				</tr>
				<tr>
					<td class="col1">16</td>
					<td class="col2">Phạm Thị Sen</td>
					<td class="col3">Nữ</td>
					<td class="col4">12/19/1990</td>
					<td class="col5">1566190086</td>
				</tr>
				<tr>
					<td class="col1">17</td>
					<td class="col2">Ngô Ngọc Thiện</td>
					<td class="col3">Nam</td>
					<td class="col4">10/13/1990</td>
					<td class="col5">1566190099</td>
				</tr>
				<tr>
					<td class="col1">18</td>
					<td class="col2">Thượng Thị Kim Thoa</td>
					<td class="col3">Nữ</td>
					<td class="col4">3/13/1993</td>
					<td class="col5">1566190102</td>
				</tr>
				<tr>
					<td class="col1">19</td>
					<td class="col2">Nguyễn Thị Thủy Tiên</td>
					<td class="col3">Nữ</td>
					<td class="col4">4/16/1988</td>
					<td class="col5">1566190112</td>
				</tr>
				<tr>
					<td class="col1">20</td>
					<td class="col2">Huỳnh Thị Thuỷ Tiên</td>
					<td class="col3">Nữ</td>
					<td class="col4">2/17/1991</td>
					<td class="col5">1566190111</td>
				</tr>
				<tr>
					<td class="col1">21</td>
					<td class="col2">Lò Thị Tình</td>
					<td class="col3">Nữ</td>
					<td class="col4">11/28/1992</td>
					<td class="col5">1566190115</td>
				</tr>
				<tr>
					<td class="col1">22</td>
					<td class="col2">Nguyễn Thị Thùy Trang</td>
					<td class="col3">Nữ</td>
					<td class="col4">1/24/1991</td>
					<td class="col5">1566190120</td>
				</tr>
				<tr>
					<td class="col1">23</td>
					<td class="col2">Nguyễn Thị Ngọc Tú</td>
					<td class="col3">Nữ</td>
					<td class="col4">9/26/1991</td>
					<td class="col5">1566190124</td>
				</tr>
				<tr>
					<td class="col1">24</td>
					<td class="col2">Lô Hạ Uyên</td>
					<td class="col3">Nữ</td>
					<td class="col4">12/18/1991</td>
					<td class="col5">1566190133</td>
				</tr>
				<tr>
					<td class="col1">25</td>
					<td class="col2">Phan Hồng Vân</td>
					<td class="col3">Nữ</td>
					<td class="col4">3/22/1991</td>
					<td class="col5">1566190136</td>
				</tr>
				<tr>
					<td class="col1">26</td>
					<td class="col2">Phạm Thị Thanh Xuân</td>
					<td class="col3">Nữ</td>
					<td class="col4">5/8/1988</td>
					<td class="col5">1566190144</td>
				</tr>
				<tr>
					<td class="col1">27</td>
					<td class="col2">Võ Ngọc Yến</td>
					<td class="col3">Nữ</td>
					<td class="col4">1/1/1993</td>
					<td class="col5">1566190146</td>
				</tr>
				<tr>
					<td class="col1">28</td>
					<td class="col2">Bùi Thị Mỹ Tâm</td>
					<td class="col3">Nữ</td>
					<td class="col4">6/2/1987</td>
					<td class="col5">1466190117</td>
				</tr>
				<tr>
					<td class="col1">29</td>
					<td class="col2">Lưu Thiện Bình</td>
					<td class="col3">Nam</td>
					<td class="col4">11/14/1983</td>
					<td class="col5">1566190147</td>
				</tr>
				<tr>
					<td class="col1">30</td>
					<td class="col2">Trần Thảo Sương</td>
					<td class="col3">Nữ</td>
					<td class="col4">21/6/1981</td>
					<td class="col5">1366190116</td>
				</tr>
				<tr>
					<td class="col1">31</td>
					<td class="col2">Đặng Thị Đông</td>
					<td class="col3">Nữ</td>
					<td class="col4">23/2/1986</td>
					<td class="col5">1566190015</td>
				</tr>
			</table>
		</div>
	</div>

</div>