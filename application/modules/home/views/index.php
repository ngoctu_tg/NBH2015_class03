<div class="slider-banner full-width">
	<div class="slider-revolt full-width">
		<div class="banner">
			<div id="owl-example" class="owl-carousel">
				<div> <img src="assets/images/N04.jpg" alt="slidebg4"> </div>
				<div> <img src="assets/images/N09.jpg" alt="slidebg9"> </div>
				<div> <img src="assets/images/N01.jpg" alt="slidebg1"> </div>
				<div> <img src="assets/images/N02.jpg" alt="slidebg2"> </div>
				<div> <img src="assets/images/N03.jpg" alt="slidebg3"> </div>
				<div> <img src="assets/images/N05.jpg" alt="slidebg5"> </div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<p class="welcome-text full-width">CHÀO MỪNG CÁC BẠN TÂN SINH VIÊN ĐẾN VỚI DIỄN ĐÀN LỚP 03<br>KHOA NHẬT BẢN HỌC KHÓA 2015</p>
	<p class="description-class full-width">
		Đây là Website tự xây dựng của lớp N3 khoa Nhật Bản Học khóa 2015. <br>
		Mọi thông báo hoặc lịch học cụ thể sẽ được cập nhật trên website này hoặc các bạn có thể theo dõi các thông báo chung trên website chính của khoa 
		<a href="http://nhatban.hcmussh.edu.vn/" class="link-group">http://nhatban.hcmussh.edu.vn/</a><br>
		Group trao đổi thảo luận trên Facebook: 
		<a href="https://www.facebook.com/groups/nbh2015n3/" class="link-group">https://www.facebook.com/groups/nbh2015n3/</a><br>
		Hoặc các bạn có thể liên hệ Lớp Trưởng: <br>
		Nguyễn Thị Ngọc Tú - Email: ngoctutg@gmail.com


	</p>

	<p class="face-lable full-width">HOẠT ĐỘNG LỚP NHẬT 3 (NĂM 2016)</p>
	<div class="list-face-student full-width" style="margin-bottom: -40px;">
		<div id="owl-album-active" class="owl-carousel">
			<div> 
				<img src="assets/images/1.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">08/03/2016</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/2.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">08/03/2016</div>
				</div>
			</div>
		</div>
	</div>

	<div class="list-face-student full-width">
		<div id="owl-album-active2" class="owl-carousel">
			<div> 
				<img src="assets/images/lop1.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 01</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop2.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 02</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop3.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 03</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop4.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 04</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop5.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 05</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop6.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 06</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop7.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 07</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop8.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 08</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop9.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 09</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop10.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 10</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop11.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 11</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop12.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 12</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop13.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 13</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop14.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 14</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop15.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 15</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop16.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 16</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop17.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 17</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop18.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 18</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop19.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 19</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop20.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 20</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop21.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 21</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/lop22.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">party 07/05/2016 - 22</div>
				</div>
			</div>
		</div>
	</div>

	<p class="face-lable full-width">THÀNH VIÊN LỚP NHẬT 3</p>
	<div class="list-face-student full-width">
		<div id="owl-listface-student" class="owl-carousel">
			<div> 
				<img src="assets/images/nhu-thao.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Như Thảo</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/cam-quyen.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Cẩm Quyên</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/duc-quang.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Đức Quang</div>
				</div>
			</div>			
			<div> 
				<img src="assets/images/ha-uyen.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Hạ Uyên</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/hung-duy.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Hùng Duy</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/thuy-tien.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Thuỷ Tiên</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/tu.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Ngọc Tú</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/khanh.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Tuyết Khanh</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/le-quyen.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Lê Quyên</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/hai-linh.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Hải Linh</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/ngoc-thien.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Ngọc Thiện</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/phep.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Phạm T.Phép</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/quoc-cuong.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Quốc Cường</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/sen.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Phạm T.Sen</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/tinh.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Lò T.Tình</div>
				</div>
			</div>
			<div> 
				<img src="assets/images/yen-nhi.jpg" alt="student1"> 
				<div class="label-name-sv">
					<div class="text-name">Yến Nhi</div>
				</div>
			</div>
		</div>
	</div>

	<!--<?php if($items) { ?>
	<div class="list-face-student full-width">
		<div id="owl-listface-student" class="owl-carousel">
			<?php foreach ($items as $k => $v) { ?>
				<div class="item"> 
					<img class="img-responsive" src="<?=resizeImage(PATH_URL.DIR_UPLOAD_TEACHERS.$v->image,170, 125)?>">
					<img src="assets/images/student.jpg" alt="student1"> 
					<div class="label-name-sv">
						<div class="text-name">Ngọc Tú</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
	<?php } ?>-->
</div>

