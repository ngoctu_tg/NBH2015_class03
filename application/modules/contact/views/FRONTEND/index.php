<div class="bm-height">
	<div class="container">
		<p class="tieude-tkb full-width">LIÊN HỆ</p>
		<p style="text-align: center; font-size: 20px; margin-top: 30px; margin-bottom: 40px;">
			Mọi liên hệ các bạn có thể điền vào form bên dưới, vui lòng điền đầy đủ thông tin (tên, email của các bạn, số điện thoại, câu hỏi thắc mắc). 
			Thư sẽ được gửi đến hộp mail của lớp trưởng: <span style="color: #374799; font-weight: bold; font-size: 19px; text-decoration: underline;">ngoctutg@gmail.com </span><br>
		</p>

		<form id="contact_form" action="" method="POST" enctype="multipart/form-data" class="full-width">
			<fieldset>
				<input type="hidden" value="<?=$this->security->get_csrf_hash()?>" id="csrf_token" name="csrf_token" />
			
				<input id="name" class="validate[required] text-input input" name="name" type="text" value="" size="30" placeholder="Name" /><span class="red"> ※</span> <br>

				<input id="email" class="validate[required,custom[email]] input" name="email" type="text" value="" size="30" placeholder="Email" /><span class="red"> ※</span> <br>
				
				<input id="phone" class="validate[required] validate[custom[phone]] text-input input" name="phone" type="text" value="" size="30" placeholder="Phone number" /><span class="red"> ※</span> <br>

				<textarea class="validate[required] text-input" id="inquiry_content" name="inquiry_content" rows="7" cols="30" placeholder="Content" ></textarea><br>
			</fieldset>
			<div class="btn-send">
				<input type="submit" id="send-contact" value="Send">
			</div><!-- btn-send -->
		</form><!-- contact_form -->

	</div>
</div>


<link rel="stylesheet" href="<?=PATH_URL?>assets/css/validationEngine.jquery.css" type="text/css"/>
<script src="<?=PATH_URL?>assets/js/validation_engine/jquery-1.8.2.min.js" type="text/javascript">
</script>
<script src="<?=PATH_URL?>assets/js/validation_engine/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=PATH_URL?>assets/js/validation_engine/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?=PATH_URL?>assets/css/validationEngine.jquery.css"/>

<script type="text/javascript" src="<?=PATH_URL?>assets/js/sweetalert2.min.js"></script> 
<link rel='stylesheet'href='<?= PATH_URL ?>assets/css/frontend/showLoading.css' type='text/css' media='all'/>
<script type='text/javascript' src='<?= PATH_URL ?>assets/js/frontend/jquery.showLoading.js'></script>

<script type="text/javascript">
	$('form#contact_form').validationEngine(
        'attach', {
            scroll: false,
            fadeDuration: 0.3,
            focusFirstField : true,
            maxErrorsPerField: 1,
            onValidationComplete: function(form, status){
                if (status == true) {
					$("#contact_form").showLoading();
                    $cus = $.post("<?=PATH_URL ?>" + "contact/send_contact", $("#contact_form").serialize());
                    $cus.done(function(data) {
                    	//alert(data);
                        if(data == 'success') {
							$("#contact_form").hideLoading();
                        	swal({ 
								title: "Gửi Thành Công!!!",
								text: "Xin vui lòng chờ câu trả lời!",
								type: "success" 
							},
							function(){
								window.location.href = '<?=PATH_URL?>contact';
							});
                        } else {
							$("#contact_form").hideLoading();
                        	swal(
                        		"Lỗi!!!", 
                        		"Không thể gửi mail!", 
                        		"error"
                        	)
                        }
                    });
                }
            }
        }
    );

</script>