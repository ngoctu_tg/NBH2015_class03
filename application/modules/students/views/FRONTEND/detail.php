<div class="big-name"><i class="icon-sun"></i><?= CutTextJP($item[0]->title,20,30,20); ?></div>
<div class="hr-blue full-width"></div>
<div class="infor-content full-width">
	<?= $item[0]->description; ?>
	<img class="img-responsive" src="<?=PATH_URL.DIR_UPLOAD_STUDENTS.$item[0]->image; ?>">
	<?php if($item[0]->content){ ?>
		<i class="cmt-top"></i>
		<?= parserEditorHTML($item[0]->content); ?>
		<i class="cmt-bottom"></i>
	<?php } ?>
</div>