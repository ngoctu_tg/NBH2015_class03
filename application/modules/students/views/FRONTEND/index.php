<link rel='stylesheet'href='<?= PATH_URL ?>assets/css/frontend/showLoading.css' type='text/css' media='all'/>
<script type='text/javascript' src='<?= PATH_URL ?>assets/js/frontend/jquery.showLoading.js'></script>

<div class="like-teacher full-width">
	<div class="container">
		<p>☆<span>教員紹介</span>☆</p>
	</div><!-- container -->
</div><!-- like-teacher -->

<div class="wrapper-content full-width">
	<div class="container">
		<div class="student-wrapper full-width">
			<div class="comments-title full-width">
			<p><?php if($this->uri->segment(2)=='1'){ print '日本人'; } elseif ($this->uri->segment(2)=='2'){ print '過去在籍'; } elseif ($this->uri->segment(2)=='3'){ print 'ベトナム人'; } elseif ($this->uri->segment(2)==''){ print '教員紹介'; } ?></p>
			</div>

			<div class="teacher-main full-width">
			<?php if($items){ ?>
				<div class="teacher-list-left">
					<ul class="full-width">
						<div id="demo">
							<section id="examples">
								<div class="content mCustomScrollbar" data-mcs-theme="minimal">
										<?php foreach ($items as $k => $v): ?>
											<li data-id="<?= $v->id; ?>" class="<?= $k == 0 ? 'active' : ''; ?> full-width">
												<a onclick="loadTable(<?= $v->id; ?>);">
													<div class="list-custom full-width">
														<div class="teacher-bg-img">
															<img class="img-responsive" src="<?=resizeImage(PATH_URL.DIR_UPLOAD_STUDENTS.$v->image,76, 56)?>">
														</div><!-- teacher-bg-img -->
														<div class="teacher-name"><?= CutTextJP($v->title, 20,18,20); ?></div>
													</div><!-- list-custom -->
												</a>
											</li>
										<?php endforeach; ?>
									
								</div>
							</section>
						</div>
					</ul>
				</div>
				<?php } else { ?>
					<p style="text-align: center;">データがありません。</p>
				<?php } ?>
				<?php if($items){ ?>
				<div class="teacher-information-right" id="teachers-content">
					<div class="big-name"><i class="icon-sun"></i><?= CutTextJP($items[0]->title, 20,30,20); ?></div>
					<div class="hr-blue full-width"></div>
					<div class="infor-content full-width">
						<?= $items[0]->description; ?>
						<img class="img-responsive" src="<?=PATH_URL.DIR_UPLOAD_STUDENTS.$items[0]->image; ?>">
						<?php if($items[0]->content){ ?>
							<i class="cmt-top"></i>
							<?= parserEditorHTML($items[0]->content); ?>
							<i class="cmt-bottom"></i>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div><!-- teacher-main -->
		</div>
	</div><!-- container -->
</div><!-- wrapper-content -->

<!-- custom scrollbar plugin -->
<script type="text/javascript" src="<?=PATH_URL?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript">
	function loadTable (id) {
		$("#teachers-content").showLoading();
		$("#teachers-content").load("<?=PATH_URL?>students/detail/" + id, 
		{ 
		    csrf_token: "<?=$this->security->get_csrf_hash()?>",
		}, function() {
			$("#teachers-content").hideLoading();
			$('#examples li').removeClass('active');
			$('#examples li[data-id='+id+']').addClass('active');
		});
	}

	 $(document).ready(function() {
	 	$(".mCustomScrollbar").mCustomScrollbar({
	        theme:"minimal"
	    });
	 });
	
</script>