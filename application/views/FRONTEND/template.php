<!DOCTYPE html>
<html lang="en">
<head>
	<title>Lớp N3 [Khoa Nhật Bản Học]-Khóa 2015</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no" />

	<!-- Default Theme -->
	<link rel="stylesheet" type="text/css" href="<?=PATH_URL?>assets/awesome/css/font-awesome.css" >
	<link rel="stylesheet" type="text/css" href="<?=PATH_URL?>assets/css/reset.css" >
	<link rel="stylesheet" type="text/css" href="<?=PATH_URL?>assets/css/styles.css" >
	<link rel="stylesheet" type="text/css" href="<?=PATH_URL?>assets/css/responsive.css" >
	<script type="text/javascript" src="<?=PATH_URL?>assets/js/jquery-1.11.3.min.js"></script>
	
	<link rel="stylesheet" href="<?=PATH_URL?>assets/css/scroller/jquery.mCustomScrollbar.css" />
	<link rel="stylesheet" type="text/css" href="<?=PATH_URL?>assets/css/sweetalert2.css">

	<!-- Important Owl stylesheet -->
	<link rel="stylesheet" href="<?=PATH_URL?>assets/css/owl-carousel/owl.carousel.css">
	<!-- Default Theme -->
	<link rel="stylesheet" href="<?=PATH_URL?>assets/css/owl-carousel/owl.theme.css">
	<!-- Include js plugin -->
	<script src="<?=PATH_URL?>assets/js/owl-carousel/owl.carousel.js"></script>
	<script src="<?=PATH_URL?>assets/js/scroller/jquery.mCustomScrollbar.concat.min.js"></script>

	<script type="text/javascript">
		var root = '<?=PATH_URL?>';
		var token_value = "<?=$this->security->get_csrf_hash()?>";
	</script>
	
</head>

<body>
	<header>
		<div class="container">
			<div class="dv-top full-width">
				<a href="<?=PATH_URL?>" class="logo">
					<img class="img-responsive" src="<?=PATH_URL?>assets/images/logo1.png" alt="Logo">
				</a><!-- logo -->
				<div class="top-nav">
					<div class="menu">
						<ul class="full-width">
							<li class="<?php if($this->uri->segment(1)==''){ print 'active'; }?>"><a href="<?=PATH_URL?>">Trang Chủ</a></li>
							<li class="<?php if($this->uri->segment(1)=='thongbao'){ print 'active'; }?>"><a href="<?=PATH_URL?>thongbao">Thông Báo</a></li>
							<li class="<?php if($this->uri->segment(1)=='bieumau'){ print 'active'; }?>"><a href="<?=PATH_URL?>bieumau">Biểu Mẫu</a></li>
							<li class="<?php if($this->uri->segment(1)=='tkb'){ print 'active'; }?>"><a href="<?=PATH_URL?>tkb">Thời Khóa Biểu</a></li>
							<li class="<?php if($this->uri->segment(1)=='dssv'){ print 'active'; }?>"><a href="<?=PATH_URL?>dssv">Danh Sách Lớp</a></li>
							<li class="<?php if($this->uri->segment(1)=='contact'){ print 'active'; }?>"><a href="<?=PATH_URL?>contact">Liên Hệ</a></li>
						</ul>
					</div><!-- menu -->
				</div><!-- top-nav -->
			</div><!-- full-width -->
		</div><!-- container -->
	</header><!-- header -->
	

	<div id="content" class="full-width">
		<?=$content?>
	</div><!-- content -->

	<footer>
		<div class="footer-menu full-width">
			<div class="container">
				<ul class="full-width">
					<li class="<?php if($this->uri->segment(1)==''){ print 'active'; }?>"><a href="<?=PATH_URL?>">Trang Chủ</a></li>
					<li class="<?php if($this->uri->segment(1)=='thongbao'){ print 'active'; }?>"><a href="<?=PATH_URL?>thongbao">Thông Báo</a></li>
					<li class="<?php if($this->uri->segment(1)=='bieumau'){ print 'active'; }?>"><a href="<?=PATH_URL?>bieumau">Biểu Mẫu</a></li>
					<li class="<?php if($this->uri->segment(1)=='tkb'){ print 'active'; }?>"><a href="<?=PATH_URL?>tkb">Thời Khóa Biểu</a></li>
					<li class="<?php if($this->uri->segment(1)=='dssv'){ print 'active'; }?>"><a href="<?=PATH_URL?>dssv">Danh Sách Lớp</a></li>
					<li class="<?php if($this->uri->segment(1)=='contact'){ print 'active'; }?>"><a href="<?=PATH_URL?>contact">Liên Hệ</a></li>
				</ul>
			</div><!-- container -->
		</div><!-- footer-menu -->

		<div class="copy-right full-width">
			<p>Nhật Bản Học - Lớp N3 © 2015 | All Rights Reserved</p>
		</div><!-- copy-right -->
	</footer><!-- footer -->
	<script src="<?=PATH_URL?>assets/js/scroller/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="<?=PATH_URL?>assets/js/main.js"></script>
</body>
</html>