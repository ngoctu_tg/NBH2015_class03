/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50625
Source Host           : localhost:3306
Source Database       : nbh2015_class03

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2016-03-02 15:28:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `jv-it_students`
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_students`;
CREATE TABLE `jv-it_students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `type` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `delete` tinyint(4) DEFAULT '0',
  `mssv` varchar(255) DEFAULT NULL,
  `ngaysinh` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_students
-- ----------------------------
INSERT INTO `jv-it_students` VALUES ('1', '', 'test', '', '', '2', '1', '1', null, '0', '127', '1221323');
