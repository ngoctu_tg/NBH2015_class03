/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50625
Source Host           : localhost:3306
Source Database       : nbh2015_class03

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2016-02-22 16:52:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin_nqt_groups`
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_groups`;
CREATE TABLE `admin_nqt_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_groups
-- ----------------------------
INSERT INTO `admin_nqt_groups` VALUES ('1', 'Root', '0|rwd,2|rwd,1|rwd,4|rwd,3|rwd,185|rwd,12|rwd,8|rwd,7|rwd,84|rwd,83|rwd,105|rwd,118|rwd,121|rwd,150|rwd,155|rwd,172|rwd', '1', '2012-08-28 14:51:26');

-- ----------------------------
-- Table structure for `admin_nqt_logs`
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_logs`;
CREATE TABLE `admin_nqt_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `function` varchar(50) NOT NULL,
  `function_id` int(11) NOT NULL,
  `field` varchar(50) NOT NULL,
  `type` varchar(20) NOT NULL,
  `old_value` text,
  `new_value` text NOT NULL,
  `account` varchar(50) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_logs
-- ----------------------------
INSERT INTO `admin_nqt_logs` VALUES ('1', 'static_pages', '1', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-19 11:13:22');
INSERT INTO `admin_nqt_logs` VALUES ('2', 'static_pages', '1', 'content', 'Update', 'test', '<table class=\"table-tkb full-width\">\r\n	<tbody>\r\n		<tr>\r\n			<th>Thời Gian</th>\r\n			<th>Thứ 2</th>\r\n			<th>Thứ 3</th>\r\n			<th>Thứ 4</th>\r\n			<th>Thứ 5</th>\r\n			<th>Thứ 6</th>\r\n			<th>Thứ 7</th>\r\n		</tr>\r\n		<tr>\r\n			<td>17h45~20h45</td>\r\n			<td>Địa lý dân cư NB<br />\r\n				<span style=\"color: #374799;\">P. D407 </span><br />\r\n				(Đinh Tiên Hoàng)<br />\r\n				(bắt đầu từ 22/2/2016)<br />\r\n				TS. Nguyễn Văn Luyện</td>\r\n			<td>Nhật Nghe Nói 1<br />\r\n				<span style=\"color: #374799;\">P.003 </span><br />\r\n				(Võ Thị Sáu)<br />\r\n				(bắt đầu từ 23/2/2016)<br />\r\n				ThS. Nguyễn Đình Ngọc Vân</td>\r\n			<td>VH Phương Đông<br />\r\n				<span style=\"color: #374799;\">P. D407 </span><br />\r\n				(ĐinhTiên Hoàng)<br />\r\n				(bắt đầu từ 24/2/2016)<br />\r\n				PGS.TS Lê Giang</td>\r\n			<td>Nhật Đọc hiểu 1<br />\r\n				<span style=\"color: #374799;\">P.003 </span><br />\r\n				(Võ Thị Sáu)<br />\r\n				(bắt đầu từ 25/2/2016)<br />\r\n				ThS. Đào Thị Hồ Phương</td>\r\n			<td>Quan hệ Quốc tế PĐ<br />\r\n				<span style=\"color: #374799;\">P. 407 </span><br />\r\n				(ĐinhTiên Hoàng)<br />\r\n				(bắt đầu từ 26/2/2016)<br />\r\n				ThS. Nguyễn Tuấn Khanh</td>\r\n			<td>Nhật Ngữ pháp 1<br />\r\n				<span style=\"color: #374799;\">P. 003 </span><br />\r\n				(Võ Thị Sáu)<br />\r\n				(b.đầu từ 27/2/2016)<br />\r\n				ThS. Nguyễn Vũ Kỳ</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p class=\"ghi-chu full-width\"><span class=\"full-width\">GHI CHÚ:</span><br />\r\n	CS ĐINH TIÊN HOÀNG: 10 - 12 Đinh Tiên Hoàng, Quận 1,TP.HCM<br />\r\n	CS VÕ THỊ SÁU: 95 Đinh Tiên Hoàng, P13, Quận Bình Thạnh, TP.HCM</p>', 'admin', '::1', '2016-02-19 14:50:13');
INSERT INTO `admin_nqt_logs` VALUES ('3', 'static_pages', '1', 'created', 'Update', '2016-02-19 11:13:22', '2016-02-19 14:50:13', 'admin', '::1', '2016-02-19 14:50:13');
INSERT INTO `admin_nqt_logs` VALUES ('4', 'static_pages', '2', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-19 15:05:56');
INSERT INTO `admin_nqt_logs` VALUES ('5', 'notification', '1', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-22 13:44:40');
INSERT INTO `admin_nqt_logs` VALUES ('6', 'notification', '1', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 13:47:17');
INSERT INTO `admin_nqt_logs` VALUES ('7', 'notification', '1', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 13:47:22');
INSERT INTO `admin_nqt_logs` VALUES ('8', 'notification', '2', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-22 13:47:43');
INSERT INTO `admin_nqt_logs` VALUES ('9', 'notification', '2', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 13:49:32');
INSERT INTO `admin_nqt_logs` VALUES ('10', 'notification', '2', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 13:49:36');
INSERT INTO `admin_nqt_logs` VALUES ('11', 'notification', '3', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-22 13:49:49');
INSERT INTO `admin_nqt_logs` VALUES ('12', 'notification', '3', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 13:52:48');
INSERT INTO `admin_nqt_logs` VALUES ('13', 'notification', '3', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 13:52:51');
INSERT INTO `admin_nqt_logs` VALUES ('14', 'notification', '4', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-22 13:53:03');
INSERT INTO `admin_nqt_logs` VALUES ('15', 'notification', '4', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 14:18:06');
INSERT INTO `admin_nqt_logs` VALUES ('16', 'notification', '4', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 14:18:10');
INSERT INTO `admin_nqt_logs` VALUES ('17', 'notification', '5', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-22 14:20:35');
INSERT INTO `admin_nqt_logs` VALUES ('18', 'notification', '5', 'image', 'Update', '', '2016/02/eec862bf2e03265f6482b7031edd5743.jpg', 'admin', '::1', '2016-02-22 14:27:36');
INSERT INTO `admin_nqt_logs` VALUES ('19', 'notification', '5', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 14:49:50');
INSERT INTO `admin_nqt_logs` VALUES ('20', 'notification', '5', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 14:49:54');
INSERT INTO `admin_nqt_logs` VALUES ('21', 'notification', '6', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-22 14:50:23');
INSERT INTO `admin_nqt_logs` VALUES ('22', 'notification', '7', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-22 14:50:45');
INSERT INTO `admin_nqt_logs` VALUES ('23', 'static_pages', '3', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-22 14:59:11');
INSERT INTO `admin_nqt_logs` VALUES ('24', 'static_pages', '4', 'Add new', 'Add new', '', '', 'admin', '::1', '2016-02-22 16:20:24');
INSERT INTO `admin_nqt_logs` VALUES ('25', 'notification', '6', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 16:46:49');
INSERT INTO `admin_nqt_logs` VALUES ('26', 'notification', '6', 'Delete', 'Delete', '', '', 'admin', '::1', '2016-02-22 16:46:53');
INSERT INTO `admin_nqt_logs` VALUES ('27', 'notification', '7', 'title', 'Update', 'Thông báo nộp tiền sách GK', 'Đăng ký SGK tiếng Nhật và Đăng kí chuyển điểm (hủy môn)', 'admin', '::1', '2016-02-22 16:52:05');
INSERT INTO `admin_nqt_logs` VALUES ('28', 'notification', '7', 'content', 'Update', 'đóng 288k @@', '<p>1 - Các bạn hôm nay đi học liên hệ lớp trường để lấy sách và đóng tiền.&nbsp;</p>\r\n<p>2 - Bạn nào có nhu cầu chuyển điểm hoặc đăng kí hủy môn (những môn đã học rồi) thì làm đơn để chuyển điểm hoặc hủy môn, mẫu đơn Lớp trưởng sẽ liên hệ lại văn phòng khoa và up lên website mục Biểu Mẫu cho các bạn tham khảo.</p>\r\n<p>Lớp trường: Ngọc Tú (sđt: 0126.323.9987)</p>', 'admin', '::1', '2016-02-22 16:52:05');

-- ----------------------------
-- Table structure for `admin_nqt_modules`
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_modules`;
CREATE TABLE `admin_nqt_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_function` varchar(50) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_modules
-- ----------------------------
INSERT INTO `admin_nqt_modules` VALUES ('1', 'Manager Account Group', 'admincp_account_groups', '0', '0', '2012-08-16 15:53:42');
INSERT INTO `admin_nqt_modules` VALUES ('2', 'Manager Account', 'admincp_accounts', '2', '0', '2012-08-16 15:53:42');
INSERT INTO `admin_nqt_modules` VALUES ('3', 'Manager Module', 'admincp_modules', '0', '0', '2012-08-16 15:53:42');
INSERT INTO `admin_nqt_modules` VALUES ('4', 'Manager Logs', 'admincp_logs', '1', '0', '2012-08-16 15:53:42');
INSERT INTO `admin_nqt_modules` VALUES ('83', 'News Management', 'news', '4', '0', '2015-06-14 19:36:38');
INSERT INTO `admin_nqt_modules` VALUES ('84', 'Static Pages Management', 'static_pages', '0', '1', '2015-06-14 19:36:38');
INSERT INTO `admin_nqt_modules` VALUES ('105', 'Categories Management', 'categories', '3', '0', '2015-06-20 12:51:53');
INSERT INTO `admin_nqt_modules` VALUES ('121', 'Banners Management', 'banners', '6', '0', '2015-07-04 22:32:44');
INSERT INTO `admin_nqt_modules` VALUES ('155', 'Menus Management', 'menus', '5', '0', '2015-10-28 16:25:37');
INSERT INTO `admin_nqt_modules` VALUES ('172', 'Notification', 'notification', null, '1', '2016-02-22 11:13:37');

-- ----------------------------
-- Table structure for `admin_nqt_settings`
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_settings`;
CREATE TABLE `admin_nqt_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_settings
-- ----------------------------
INSERT INTO `admin_nqt_settings` VALUES ('1', 'mail-admin', 'ngoctutg@gmail.com', '2016-02-22 14:17:52');
INSERT INTO `admin_nqt_settings` VALUES ('2', 'name-mail-admin', 'Admin', '2016-02-22 14:17:52');
INSERT INTO `admin_nqt_settings` VALUES ('3', 'subject', '[NhatBanHoc-N3]', '2016-02-22 14:17:52');
INSERT INTO `admin_nqt_settings` VALUES ('4', 'seo-title', 'Nhat Ban Hoc', '2016-02-22 14:17:52');
INSERT INTO `admin_nqt_settings` VALUES ('5', 'seo-meta-keyword', 'Nhat Ban Hoc', '2016-02-22 14:17:52');
INSERT INTO `admin_nqt_settings` VALUES ('6', 'seo-meta-description', 'Nhat Ban Hoc', '2016-02-22 14:17:52');

-- ----------------------------
-- Table structure for `admin_nqt_users`
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_users`;
CREATE TABLE `admin_nqt_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `custom_permission` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_users
-- ----------------------------
INSERT INTO `admin_nqt_users` VALUES ('1', 'root', '53fab80925e21d959402658124f71c36', '1', '2|rwd,1|rwd,4|rwd,3|rwd,185|rwd,12|rwd,8|rwd,7|rwd,84|rwd,83|rwd,105|rwd,118|rwd,121|rwd,150|rwd,155|rwd,172|rwd', '0', '1', '0', '2012-08-28 14:52:42');
INSERT INTO `admin_nqt_users` VALUES ('2', 'admin', 'af03798e4f9010c54d2eb6f386124f7e', '1', '2|rwd,1|rwd,4|rwd,3|rwd,185|rwd,12|rwd,8|rwd,7|rwd,84|rwd,83|rwd,105|rwd,118|rwd,121|rwd,150|rwd,155|rwd,172|rwd', '0', '1', '0', '2012-08-28 14:52:59');

-- ----------------------------
-- Table structure for `jv-it_banners`
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_banners`;
CREATE TABLE `jv-it_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT '1',
  `delete` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_banners
-- ----------------------------

-- ----------------------------
-- Table structure for `jv-it_categories`
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_categories`;
CREATE TABLE `jv-it_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `delete` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_categories
-- ----------------------------

-- ----------------------------
-- Table structure for `jv-it_menus`
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_menus`;
CREATE TABLE `jv-it_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `order` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `delete` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_menus
-- ----------------------------

-- ----------------------------
-- Table structure for `jv-it_news`
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_news`;
CREATE TABLE `jv-it_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `highlight` tinyint(1) DEFAULT '1',
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `delete` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_news
-- ----------------------------

-- ----------------------------
-- Table structure for `jv-it_notification`
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_notification`;
CREATE TABLE `jv-it_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `highlight` tinyint(1) DEFAULT '1',
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `delete` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_notification
-- ----------------------------
INSERT INTO `jv-it_notification` VALUES ('7', '', 'Đăng ký SGK tiếng Nhật và Đăng kí chuyển điểm (hủy môn)', null, '', '<p>1 - Các bạn hôm nay đi học liên hệ lớp trường để lấy sách và đóng tiền.&nbsp;</p>\r\n<p>2 - Bạn nào có nhu cầu chuyển điểm hoặc đăng kí hủy môn (những môn đã học rồi) thì làm đơn để chuyển điểm hoặc hủy môn, mẫu đơn Lớp trưởng sẽ liên hệ lại văn phòng khoa và up lên website mục Biểu Mẫu cho các bạn tham khảo.</p>\r\n<p>Lớp trường: Ngọc Tú (sđt: 0126.323.9987)</p>', null, '1', null, null, null, '1', '0', '2016-02-22 14:50:45', '1');

-- ----------------------------
-- Table structure for `jv-it_static_pages`
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_static_pages`;
CREATE TABLE `jv-it_static_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_static_pages
-- ----------------------------
INSERT INTO `jv-it_static_pages` VALUES ('1', 'thoikhoabieu', 'thoikhoabieu', '<table class=\"table-tkb full-width\">\r\n	<tbody>\r\n		<tr>\r\n			<th>Thời Gian</th>\r\n			<th>Thứ 2</th>\r\n			<th>Thứ 3</th>\r\n			<th>Thứ 4</th>\r\n			<th>Thứ 5</th>\r\n			<th>Thứ 6</th>\r\n			<th>Thứ 7</th>\r\n		</tr>\r\n		<tr>\r\n			<td>17h45~20h45</td>\r\n			<td>Địa lý dân cư NB<br />\r\n				<span style=\"color: #374799;\">P. D407 </span><br />\r\n				(Đinh Tiên Hoàng)<br />\r\n				(bắt đầu từ 22/2/2016)<br />\r\n				TS. Nguyễn Văn Luyện</td>\r\n			<td>Nhật Nghe Nói 1<br />\r\n				<span style=\"color: #374799;\">P.003 </span><br />\r\n				(Võ Thị Sáu)<br />\r\n				(bắt đầu từ 23/2/2016)<br />\r\n				ThS. Nguyễn Đình Ngọc Vân</td>\r\n			<td>VH Phương Đông<br />\r\n				<span style=\"color: #374799;\">P. D407 </span><br />\r\n				(ĐinhTiên Hoàng)<br />\r\n				(bắt đầu từ 24/2/2016)<br />\r\n				PGS.TS Lê Giang</td>\r\n			<td>Nhật Đọc hiểu 1<br />\r\n				<span style=\"color: #374799;\">P.003 </span><br />\r\n				(Võ Thị Sáu)<br />\r\n				(bắt đầu từ 25/2/2016)<br />\r\n				ThS. Đào Thị Hồ Phương</td>\r\n			<td>Quan hệ Quốc tế PĐ<br />\r\n				<span style=\"color: #374799;\">P. 407 </span><br />\r\n				(ĐinhTiên Hoàng)<br />\r\n				(bắt đầu từ 26/2/2016)<br />\r\n				ThS. Nguyễn Tuấn Khanh</td>\r\n			<td>Nhật Ngữ pháp 1<br />\r\n				<span style=\"color: #374799;\">P. 003 </span><br />\r\n				(Võ Thị Sáu)<br />\r\n				(b.đầu từ 27/2/2016)<br />\r\n				ThS. Nguyễn Vũ Kỳ</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p class=\"ghi-chu full-width\"><span class=\"full-width\">GHI CHÚ:</span><br />\r\n	CS ĐINH TIÊN HOÀNG: 10 - 12 Đinh Tiên Hoàng, Quận 1,TP.HCM<br />\r\n	CS VÕ THỊ SÁU: 95 Đinh Tiên Hoàng, P13, Quận Bình Thạnh, TP.HCM</p>', '0', '2016-02-19 14:50:13');
INSERT INTO `jv-it_static_pages` VALUES ('2', 'danh sach sinh vien', 'danh-sach-sinh-vien', 'testtt', '0', '2016-02-19 15:05:56');
INSERT INTO `jv-it_static_pages` VALUES ('3', 'Biểu Mẫu', 'bieu-mau', 'test', '0', '2016-02-22 14:59:11');
INSERT INTO `jv-it_static_pages` VALUES ('4', 'Lien He', 'lien-he', 'test', '0', '2016-02-22 16:20:24');
