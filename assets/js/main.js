$(document).ready(function() {
    var isiDevice = /ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase());
    var isAndroid = /android/i.test(navigator.userAgent.toLowerCase());
    var isBlackBerry = /blackberry/i.test(navigator.userAgent.toLowerCase());
    var isWindowsPhone = /windows phone/i.test(navigator.userAgent.toLowerCase());
    var isWebOS = /webos/i.test(navigator.userAgent.toLowerCase());

    $("#owl-example").owlCarousel({
        navigation : false, //prev/next button
        pagination : false, //nut tron
        autoPlay: true,
        // items : 4,
        singleItem:true,
        slideSpeed : 300,
        paginationSpeed : 500,
        navigationText: [
        "<i class='icon-chevron-left icon-white'></i>",
        "<i class='icon-chevron-right icon-white'></i>"
        ]
    });
    
    $("#owl-listface-student").owlCarousel({
        navigation : true, //prev/next button
        pagination : false, //nut tron
        autoPlay: true,
        // items : 5,
        itemsCustom : [
            [320, 1],
            [450, 2],
            [600, 3],
            [1000, 5]
        ],
        slideSpeed : 300,
        paginationSpeed : 500,
        navigationText: [
        "<i class='icon-chevron-left icon-white'></i>",
        "<i class='icon-chevron-right icon-white'></i>"
        ]
    });

    $("#owl-album-active").owlCarousel({
        navigation : true, //prev/next button
        pagination : false, //nut tron
        autoPlay: false,
        // singleItem:true,
        items : 2,
        slideSpeed : 300,
        paginationSpeed : 500,
        navigationText: [
        "<i class='icon-chevron-left icon-white'></i>",
        "<i class='icon-chevron-right icon-white'></i>"
        ]
    });

    $("#owl-album-active2").owlCarousel({
        navigation : true, //prev/next button
        pagination : false, //nut tron
        autoPlay: true,
        // singleItem:true,
        items : 4,
        slideSpeed : 100,
        paginationSpeed : 500,
        navigationText: [
        "<i class='icon-chevron-left icon-white'></i>",
        "<i class='icon-chevron-right icon-white'></i>"
        ]
    });

    // $("#content-1").mCustomScrollbar({
    //     theme:"minimal"
    // });

    jQuery('.div-jquery h4').click(function(){
        $('.div-jquery h4').removeClass('slideShow');
        $(this).addClass('slideShow');

        var tmp=jQuery(this).next();
        if(tmp.is(':hidden') === true){
            jQuery('.div-jquery article').slideUp();
            tmp.slideDown();
        }else{
            tmp.slideUp();
            $(this).removeClass('slideShow');
        }
    });

    jQuery('#notifi-custom h4').click(function(){
        $("html, body").animate({
            scrollTop: 200
        }, 600);
    });


});



function scrollAnimate(){
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
}


